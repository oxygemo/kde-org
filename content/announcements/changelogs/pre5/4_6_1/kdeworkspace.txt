commit 5949488bb84cbac2525f551b23ed499448c49814
Author: Dirk Mueller <mueller@kde.org>
Date:   Fri Feb 25 23:11:13 2011 +0100

    bump version to 4.6.1

commit 1021789c698efc4f0598c57eb3e9efcf304c77c1
Author: Hugo Pereira Da Costa <hugo@oxygen-icons.org>
Date:   Wed Feb 23 17:42:38 2011 +0100

    added protection when accessing listview model.
    
    CCBUG: 266087

commit 4701d8bdc40187297dee8cb7ac610d6e92bed728
Author: David Faure <faure@kde.org>
Date:   Mon Feb 21 21:27:29 2011 +0100

    Updated from the current Qt code, so that this compiles with libpng 1.5.1
    
    Fixes "invalid use of incomplete type 'struct png_info'" errors.
    (cherry picked from commit 0323d1c21d7b24fba71137112cd3712cbf669dad)

commit eb62d3bb58fd66b8233d2f2e4cfd29539ba3aad2
Author: Script Kiddy <scripty@kde.org>
Date:   Mon Feb 21 14:31:20 2011 +0100

    SVN_SILENT made messages (.desktop file)

commit ac434103641bca4988e6a37a7445a8ed0127a6d1
Author: Andriy Rysin <arysin@gmail.com>
Date:   Sat Feb 19 00:17:08 2011 -0500

    Compact the keyboard layout rows in kcm (try 2)

commit edb7f1907536c0173a9315426557b07d7a99fc9c
Author: Andriy Rysin <arysin@gmail.com>
Date:   Fri Feb 18 23:17:15 2011 -0500

    Compact the keyboard layout rows in kcm

commit 85f221f26e4e6b8434af8b8bc5982c29e48e0fb7
Author: Aaron Seigo <aseigo@kde.org>
Date:   Fri Feb 18 13:09:34 2011 -0800

    keep launchers at the head of the list, always
    
    BUG:264863
    BUG:266612

commit 2babeb81b002a5790ea34f6f2005eaf91fa396bd
Author: Pino Toscano <pino@kde.org>
Date:   Fri Feb 18 16:09:56 2011 +0100

    ksplash: use png_jmpbuf() instead of accessing the jmpbuf directly
    
    this should help with png 1.5
    (cherry picked from commit 7e6027ac159750cff0d5fb1f065417a47a32d8ae)

commit faae2b81f6145d6d5297f3a87cf034d8a69025a7
Author: Script Kiddy <scripty@kde.org>
Date:   Thu Feb 17 16:26:45 2011 +0100

    SVN_SILENT made messages (.desktop file)

commit 296e2c797e2c58f64a5f1a8b09739f77e950cdb0
Author: Lukas Tinkl <lukas@kde.org>
Date:   Wed Feb 16 18:17:39 2011 +0100

    treat a UPS as a battery as well

commit 06e8ec5eea47ab6f3c84892e493e8ed714cb49de
Author: Aaron Seigo <aseigo@kde.org>
Date:   Tue Feb 15 11:15:14 2011 -0800

    don't KRun the url twice when we have the "right" dest already
    
    BUG:266332

commit cdc556ccdbe7fde9f436def6791174ca9f0ed42d
Author: Thomas Lübking <thomas.luebking@web.de>
Date:   Fri Feb 4 21:30:36 2011 +0100

    export WindowGeometry config symbols
    
    BUG: 265272

commit 891a0a2a3272186c2c7a93f7a332e04809e3d4d4
Author: Thomas Lübking <thomas.luebking@gmail.com>
Date:   Mon Feb 14 20:38:31 2011 +0100

    secure m_highlightedWindows access when triggering
    explicit repaint in highlight effect

commit 8aa28760efc3a77d0e8f48b6ede142d898b44856
Author: Thomas Lübking <thomas.luebking@gmail.com>
Date:   Mon Feb 14 20:37:35 2011 +0100

    secure referenced windows in uncomposited tabbox highlighting
    
    BUG: 263250

commit e2d3cc58919790dbd2a0b44f2d98d7ef1a9dd50a
Author: Andriy Rysin <arysin@gmail.com>
Date:   Sun Feb 13 22:30:10 2011 -0500

    Fix how the keyboard layouts are set
    BUG: 258266
    FIXED-IN: 4.6.1

commit 9d3650b4fc003991f51f38c5cee729baa6355750
Author: Script Kiddy <scripty@kde.org>
Date:   Mon Feb 14 04:03:15 2011 +0100

    SVN_SILENT made messages (.desktop file)

commit 712ccdb987278e7192da4da8f2ac15a447526c14
Author: Jacopo De Simoi <wilderkde@gmail.com>
Date:   Sun Feb 13 11:45:16 2011 +0100

    GIT_SILENT: cleanup useless debug messages

commit 66de4e47024fc717089026df9fdb1fb2a4796cee
Author: Raphael Kubo da Costa <kubito@gmail.com>
Date:   Sun Feb 13 01:32:01 2011 -0200

    kinfocenter (FreeBSD): Use QFileInfo::exists() when possible.
    
    We thus avoid creating a QFile on the heap just to check if it exists.

commit c61183fabf54d70eda7600b015b64a492636ca00
Author: Raphael Kubo da Costa <kubito@gmail.com>
Date:   Sun Feb 13 01:24:51 2011 -0200

    kinfocenter (FreeBSD): Indent line correctly.
    
    GIT_SILENT

commit 5f1da85ed018d05e924b0dc8ee311d68c63f3a56
Author: Raphael Kubo da Costa <kubito@gmail.com>
Date:   Sun Feb 13 01:22:49 2011 -0200

    kinfocenter (FreeBSD): Manually read pciconf's output.
    
    GetInfo_ReadfromPipe() reportedly does not work with pciconf's
    output. For now, we just resort to reading the output by
    ourselves. Ideally, we should not even need a pipe here, and just use
    {Q,K}Process instead.
    
    Based on an original patch by Max Brazhnikov, from the KDE on FreeBSD
    team.
    
    CCMAIL: makc@freebsd.org

commit 6ef772d0cc3557d1d7fb085e8f4ff565e3bbe751
Author: Raphael Kubo da Costa <kubito@gmail.com>
Date:   Sun Feb 13 01:20:46 2011 -0200

    kinfocenter (FreeBSD): Remove checks for old PCI-related programs.
    
    /usr/X11R6 does not even exist anymore, so it does not make sense to
    look for programs there. We should only check if pciconf exists.
    
    Original patch by Max Brazhnikov from the KDE on FreeBSD team.
    
    CCMAIL: makc@freebsd.org

commit f6884983b549d8306e8427b93b5fe0af586262f6
Author: Davide Bettio <bettio@kde.org>
Date:   Sun Feb 13 01:33:47 2011 +0100

    Previous bugfix to browser widget has break resizing (and other suff). Fixed with a workaround.

commit da97a77fcd01892a7e2a1a4e8e71ab94b2ece32f
Author: David Faure <faure@kde.org>
Date:   Thu Feb 10 22:25:16 2011 +0100

    Reuse old kconf_update script to now remove kde paths from libraryPath
    (cherry picked from commit 2aaf5663662810a56e7f16471c6ff5bf0771c629)

commit 9d7eb8f716873d2079039d336c8f9a21b2fae0fa
Author: David Faure <faure@kde.org>
Date:   Thu Feb 10 21:57:26 2011 +0100

    Remove code that writes 4.7\libraryPath into ~/.config/Trolltech.conf
    
    This was meant to make qt-only apps find kde plugins like oxygen, in a
    kde desktop, but this is achieved by having startkde export QT_PLUGIN_PATH
    anyway, these days. And the problem with writing to Trolltech.conf is that
    it breaks having two versions of KDE on top of the same Qt. A BIC in
    liboxygensyle.so and your kde apps crash on startup, due to loading the
    wrong oxygen style. Discussed with RichMoore and Thiago on IRC.
    (cherry picked from commit 066fe5a12b25d2050f31c5049a25a557dd95a420)

commit 35882283f6529bcf242bc9ba75a0d4ef8ed11bb5
Author: Script Kiddy <scripty@kde.org>
Date:   Fri Feb 11 13:27:17 2011 +0100

    SVN_SILENT made messages (.desktop file)

commit acee63d5ff5831d70df19361c404032e206e247c
Author: Script Kiddy <scripty@kde.org>
Date:   Thu Feb 10 13:49:23 2011 +0100

    SVN_SILENT made messages (.desktop file)

commit 7d537a3932d83f3f4669f35b7520809ffa0abbcc
Author: Script Kiddy <scripty@kde.org>
Date:   Tue Feb 8 14:29:15 2011 +0100

    SVN_SILENT made messages (.desktop file)

commit cc616b059d7affd7e9d95676f437ace0703b9668
Author: Lukas Tinkl <ltinkl@redhat.com>
Date:   Tue Feb 8 13:54:37 2011 +0100

    prefer nvidia_backlight over sony
    
    CCBUG: 257948

commit ad7f5c283b76a85efe099eb8226f17e6525c7d1b
Author: Dr. Robert Marmorstein <robert@narnia.homeunix.com>
Date:   Mon Feb 7 21:05:40 2011 -0500

    Fix kwin crash when using animated effects.
    
    BUG: 263383

commit 9c1ca506fc0df1e73586ab44cb02ecd0d7dc3db2
Author: Andre Woebbeking <Woebbeking@kde.org>
Date:   Mon Feb 7 18:28:17 2011 +0100

    -Wparentheses
    
    CCMAIL:lamarque@gmail.com

commit eda79d1650f132b0e6c7b8416504536759c2bb63
Author: Script Kiddy <scripty@kde.org>
Date:   Sun Feb 6 12:23:07 2011 +0100

    SVN_SILENT made messages (.desktop file)

commit c640d3a60241fbf4d53608db318acf260e5956be
Author: Andriy Rysin <arysin@gmail.com>
Date:   Sat Feb 5 15:33:00 2011 -0500

    fix layout map when using indicator mode
    BUG: 264595

commit 04831d049f73bb38694d7cecc4ea170b2a26a149
Author: Thomas Lübking <thomas.luebking@web.de>
Date:   Fri Feb 4 21:28:31 2011 +0100

    remove delted windows from list. can happen and triggers kwin part of
    BUG: 265297

commit 17934f79fa691a52efc4df142e5e8712634ad21a
Author: Thomas Lübking <thomas.luebking@web.de>
Date:   Fri Feb 4 21:27:27 2011 +0100

    explicitly trigger minimal repaint on property change, otherwise broken when switching windows

commit 09492b4dd8c83056125275cf558e00ad7c4a1e0f
Author: Lamarque V. Souza <lamarque@gmail.com>
Date:   Sat Feb 5 17:50:39 2011 -0200

    Backport c5721519a0ccf8a528bdb31f051509db00e98e0c by lvsouza from master
    to the 4.6 branch:
    
    Add UpsBatteries to batterySources so that the Battery plasmoid can
    monitors them when using UPower backend.

commit 84384efb52367e720ad82ace04ac8da668afc5ec
Author: Thomas Lübking <thomas.luebking@web.de>
Date:   Sat Feb 5 20:12:00 2011 +0100

    strip qhash::operator[] from boxswitch
    
    possibly prevents leaking ItemInfos
    secure some accesses (selected_window), not sure whether this caused the unreproducable crash when used as proxy in coverswitch
    CCBUG: 253079

commit c0426cf241caa9814413664b7f530ad9648dddd3
Author: Lasse Liehu <lliehu@kolumbus.fi>
Date:   Sat Feb 5 19:51:46 2011 +0200

    Correct buddy for label Layout

commit 2e4563e92116502c119dfcf7a340545eeaf182df
Author: David Faure <faure@kde.org>
Date:   Sat Feb 5 10:45:10 2011 +0100

    Calling deleteLater on NULL gives "QCoreApplication::postEvent: Unexpected null receiver"

commit 3ee99254f183b0cef1ca8a63c111350db038fa6e
Author: Aaron Seigo <aseigo@kde.org>
Date:   Fri Feb 4 12:00:10 2011 -0800

    let the world know we've changed our sizehint

commit b3595ede07ab1b90379d968c67c688231e9b452d
Author: Aaron Seigo <aseigo@kde.org>
Date:   Fri Feb 4 11:52:56 2011 -0800

    add translation context to "New Activity" string

commit 828d87e0adf53321ee5d0bc375f69afc64f24371
Author: Script Kiddy <scripty@kde.org>
Date:   Fri Feb 4 12:29:37 2011 +0100

    SVN_SILENT made messages (.desktop file)

commit 1aebb4e73b32c4124bbbdf67e76ed93689182b11
Author: Aaron Seigo <aseigo@kde.org>
Date:   Thu Feb 3 23:20:36 2011 -0800

    use sensors name, not label which may not be unique
    
    patch by Alexey Chernov
    BUG:251145

commit 0040d4b70e2c4bc4c4fe4fb33cf0018d430a2205
Author: Aaron Seigo <aseigo@kde.org>
Date:   Thu Feb 3 23:16:12 2011 -0800

    save disabled screens, don't enable by accident
    
    based on a patch by Alexey Chernov
    BUG:264702

commit 77345cff1339ec70b312f68bb315d19ef4fbb2ac
Author: Aaron Seigo <aseigo@kde.org>
Date:   Thu Feb 3 23:04:03 2011 -0800

    quote argument
    
    patch by Alexey Chernov
    BUG:264703

commit 366f9dc8ec0e28a71456f60ae8a94a5eefaec801
Author: Aaron Seigo <aseigo@kde.org>
Date:   Thu Feb 3 23:02:38 2011 -0800

    turn off outputs configured that way
    
    patch by Alexey Chernov
    BUG:264701

commit b5edbe651115f5e5cfa8fdc155eae8d19643cea3
Author: Aaron Seigo <aseigo@kde.org>
Date:   Thu Feb 3 22:27:44 2011 -0800

    don't bother asserting
    
    the methods that call with the assertion don't seem to actually rely
    on it, and akonadi is regularly returning non-existent incidences.
    
    BUG:265354

commit 84f173fd295167a706cdaec1b0fc413a695b9413
Author: Aaron Seigo <aseigo@kde.org>
Date:   Thu Feb 3 19:32:01 2011 -0800

    update appropriately

commit 0d44a8a4c6a7c7a9d4a2c2aa8ce31805862711e6
Author: Aaron Seigo <aseigo@kde.org>
Date:   Thu Feb 3 19:29:18 2011 -0800

    make autoresize work again
    
    sometimes the applets come to us with preferredSize() == size() already
    unfortunatley, with 4.6 libplasma, this causes some "jumping around" due
    to an unfortunate handling of the sizeHintChanged signal by PopupApplet,
    but i have a fix for that too.(tm)

commit 7794e4718c16a512ad3b30f9a9bd130b74d32f5d
Author: Aaron Seigo <aseigo@kde.org>
Date:   Thu Feb 3 11:40:39 2011 -0800

    return, don't assert, on incidence fetch failure
    
    these asserts get compiled out of production builds, and it is indeed
    possible with Akonadi, at least currently, to get null results back.
    so just live with that.
    
    BUG:265287

commit cfb2660e261dfe4bb8984e53f3f7b680161a34f1
Author: Aaron J. Seigo <aseigo@kde.org>
Date:   Fri Jan 28 23:31:23 2011 +0000

    use DesktopCorona for this
    
    svn path=/trunk/KDE/kdebase/workspace/; revision=1217835

commit 375112824488df9a827a498b512eabeb5ce0deaf
Author: Hugo Pereira Da Costa <hugo@oxygen-icons.org>
Date:   Thu Feb 3 11:44:59 2011 +0100

    Check validity of QToolButton cast before testing popupMode()
    
    BUG: 265271

commit 7a2c992da0d490d09d2f068c2c3fdb244bc3b396
Author: Script Kiddy <scripty@kde.org>
Date:   Wed Feb 2 14:01:48 2011 +0100

    SVN_SILENT made messages (.desktop file)

commit 8a5d6de2b4dad9310e58f29a21a5000ee96c9269
Author: Aaron Seigo <aseigo@kde.org>
Date:   Wed Feb 2 00:44:14 2011 -0800

    Service objects are owned by the caller
    
    BUG:264588

commit dfc2d0fe86bdc18bf8ac20aae26c1ce8a4ccc555
Author: Aaron Seigo <aseigo@kde.org>
Date:   Tue Feb 1 12:54:44 2011 -0800

    try to avoid doing any work when we are deleting

commit 2b7efee4acf52a9a80a71e1272cae8e2478e7210
Author: Aaron Seigo <aseigo@kde.org>
Date:   Tue Feb 1 12:54:25 2011 -0800

    make them all uniqueconnections

commit 6cf10ba3c0fa78f23066f3b0756969a2bb3f6467
Author: Aaron Seigo <aseigo@kde.org>
Date:   Tue Feb 1 12:54:10 2011 -0800

    simplification

commit 817ae4b739aee32098a3ea3715dc772518406508
Author: Aaron Seigo <aseigo@kde.org>
Date:   Tue Feb 1 11:29:42 2011 -0800

    try not to crash, a noble goal.
    
    BUG:265089

commit 441ad0f8c2b292215d0247e404f21a303f206966
Author: Aaron Seigo <aseigo@kde.org>
Date:   Tue Feb 1 10:35:00 2011 -0800

    toggle the activity manager
    
    BUG:265075

commit 5fe9fde09b57c64faf7ca6da768f6afaf9b89318
Author: Frederik Schwarzer <schwarzerf@gmail.com>
Date:   Mon Jan 31 21:12:45 2011 +0100

    fix plural handling
    
    CCMAIL: kde-i18n-doc@kde.org

commit 4ed7b9020fe5f52aed1413ec15e3f035afc22863
Author: Lukas Tinkl <lukas@kde.org>
Date:   Mon Jan 31 16:54:57 2011 +0100

    add Dell backlight interface
    
    CCBUG:257948

commit c71259cb52dd6f0cfb36ef8414fbcae7695e03e9
Author: Dario Freddi <drf@kde.org>
Date:   Mon Jan 31 00:12:26 2011 +0100

    Move cache clearing out of ActionPool's destructor, and force Core to trigger cache clearing on destruction.
    
    ActionPool is a global object, hence the destructor won't be called after Core has been deleted. For this reason, clearing should occur inside Core itself,
    or upon a restart, thr cache could be corrupted.
    
    BUG: 264565
    FIXED-IN: 4.6.1
    
    (cherry picked from commit f4517e2bb59ff6bc4c1b38724f49aa54fec6c0f9)

commit b98c44631491295335336e307e4939061e60a318
Author: Aaron Seigo <aseigo@kde.org>
Date:   Sun Jan 30 13:14:57 2011 -0800

    layout fixes for delayed and hidden
    
    * shorten the delay to one second for delayed check
    * if the mouse is still over the area, just reset the timer
    * use the lovely method for redoing the hidden layout, fixes unwanted space when an app quits, e.g.

commit 3c75313375b77360a46cd48029138ac103045feb
Author: Aaron Seigo <aseigo@kde.org>
Date:   Sun Jan 30 13:14:06 2011 -0800

    be sure to update all icons when created

commit 918f57dec00ec868698451acdcd239a073d31072
Author: Aaron Seigo <aseigo@kde.org>
Date:   Sun Jan 30 11:50:44 2011 -0800

    rework DataEngine handling in the Calendar widget
    
    * CalendarTable now handles the DataEngine on its own
    * releases engine when no longer needed, and only grabs it when needed
      big result is that akonadi no longer triggered if events are off,
      for instance

commit 8fac9a53e35f64f70773130280c7cdac98f70fd7
Author: Aaron Seigo <aseigo@kde.org>
Date:   Sun Jan 30 11:40:43 2011 -0800

    no longer need to set the DatEngine explicitly

commit fba568fc0a48a69de151c7277989f6b144ec0c06
Author: Aaron Seigo <aseigo@kde.org>
Date:   Sun Jan 30 11:40:01 2011 -0800

    imlpement configChanged

commit c514910cef68c614286be7135e0d22d9226dacfa
Author: Dario Freddi <drf@kde.org>
Date:   Sun Jan 30 15:00:01 2011 +0100

    Add a new method to ActionPool, init(), to load on startup actions which require instant load.
    
    Some actions, such as DPMS, require to be initialized even if no profile explicitly asked for the action itself,
    as it might be necessary to perform some changes when the action is disabled.
    
    CCBUG: 264730
    FIXED-IN: 4.6.1
    
    (cherry picked from commit 5dd9f08b1849800989778d4ca82f91b6977acfff)

commit 925ecf0f743fa3d7c17d90094fcf5bcf55d276f1
Author: Dario Freddi <drf@kde.org>
Date:   Sun Jan 30 14:59:10 2011 +0100

    Add a new property, X-KDE-PowerDevil-Action-ForceInstantLoad.
    
    This property will be used by actions which need loading on startup regardless of profile usage.
    
    (cherry picked from commit 04da0fd452a4f707bfc72f33cb76b0c33afb343f)

commit 397684cbf750b63a3a84a65e0eb1a44a22a483f0
Author: Dario Freddi <drf@kde.org>
Date:   Sun Jan 30 14:55:53 2011 +0100

    Move DPMS handling in the constructor.
    
    This makes more sense as
    
    1. With the previous approach, DPMS support was queried everytime a profile changed, now it is queried on startup only.
    2. It allows to disable DPMS on startup even if a profile has not loaded the action yet.
    
    (cherry picked from commit 8db343c4b8c511b6d85dbe0ffebc039280836280)

commit 96ceaf10305f69eca8ef1989da5472159ef29639
Author: Dario Freddi <drf@kde.org>
Date:   Sun Jan 30 14:54:11 2011 +0100

    Use KServiceTypeTrader's query features to improve the dynamic action load.
    
    (cherry picked from commit 2804307db01040bf66e66985272ed92158b1fabd)

commit 524ba3045663afc184de2270544988574272700f
Author: Aaron J. Seigo <aseigo@kde.org>
Date:   Fri Jan 28 21:56:41 2011 +0000

    set status when menu hides
    CCBUG:264679
    
    svn path=/branches/KDE/4.6/kdebase/workspace/; revision=1217822

commit 89b946b4c03750536de539af1aa218413e4388ca
Author: Lukáš Tinkl <lukas@kde.org>
Date:   Fri Jan 28 16:33:16 2011 +0000

    fix message extraction
    
    svn path=/branches/KDE/4.6/kdebase/workspace/; revision=1217784

commit 57a927ff69f5b1673e441c87ac59550f23ac3594
Author: Lamarque Vieira Souza <lamarque@gmail.com>
Date:   Thu Jan 27 23:05:29 2011 +0000

    Backport r1217574 by lvsouza from trunk to the 4.6 branch:
    
    Rounds value instead of truncating it. This fix a problem with increasing
    brightness key not working sometimes.
    CCBUG: 264534
    
    svn path=/branches/KDE/4.6/kdebase/workspace/; revision=1217581

commit 2f26c2ac305cb5b739899a48288f521308f2129d
Author: Aaron J. Seigo <aseigo@kde.org>
Date:   Thu Jan 27 23:04:48 2011 +0000

    abstractItem can be null
    BUG:264079
    
    svn path=/branches/KDE/4.6/kdebase/workspace/; revision=1217580

commit 11ff6e8b976c3fabc57939a141b809214194e6c3
Author: Sebastian Kügler <sebas@kde.org>
Date:   Thu Jan 27 20:32:14 2011 +0000

    single click opens the config
    
    activated() is checked against KGlobalSettings::singleClick(), clicked() isn't.
    
    FIXED-IN:4.6.1
    BUG:259470
    
    
    svn path=/branches/KDE/4.6/kdebase/workspace/; revision=1217549

commit 2ade48885a354f2ac88a1f4102752df416f95d1c
Author: Aaron J. Seigo <aseigo@kde.org>
Date:   Thu Jan 27 19:30:39 2011 +0000

    resort to defaults if the image set no longer exists
    
    svn path=/branches/KDE/4.6/kdebase/workspace/; revision=1217533

commit 12a6d845e55292f79e5f12883bac23e70293e208
Author: Yuri Chornoivan <yurchor@ukr.net>
Date:   Thu Jan 27 09:16:49 2011 +0000

    Backport 1217417. Fix typo reported by Cristian Oneț: privillages->privileges. (approved by kde-i18n-doc)
    
    svn path=/branches/KDE/4.6/kdebase/workspace/; revision=1217435

commit 14c13db30084ddab0eabd4ced1d4f1c1dc8cde9c
Author: Dario Freddi <drf@kde.org>
Date:   Wed Jan 26 21:25:37 2011 +0000

    Backporting r1217364
    
    CCBUG: 263658
    
    Forgot to emit changed(false), which caused saving to happen, but notification to the user to fail.
    
    svn path=/branches/KDE/4.6/kdebase/workspace/; revision=1217365

commit 639092d4356296b8ab90aec264665ca599c6a894
Author: Dario Freddi <drf@kde.org>
Date:   Wed Jan 26 21:20:49 2011 +0000

    Backport r1217362
    
    Use some saner defaults (thanks, Macbook) and fix some small leftover bugs from yesterday's frantic commit.
    
    svn path=/branches/KDE/4.6/kdebase/workspace/; revision=1217363

commit f1f12def7a5b0e0ebb999c7cdb3828738d5177c6
Author: Will Stephenson <wstephenson@kde.org>
Date:   Wed Jan 26 21:01:37 2011 +0000

    Backport r1217356 (fix missing icon)
    
    svn path=/branches/KDE/4.6/kdebase/workspace/; revision=1217359

commit a6d1e5e1397555b317c84fe4e3266e6892074bf6
Author: Will Stephenson <wstephenson@kde.org>
Date:   Wed Jan 26 14:25:13 2011 +0000

    Remove MALLOC_CHECK_ from 4.6 branch
    CCMAIL: release-team@kde.org
    CCMAIL: kde-packager@kde.org
    
    svn path=/branches/KDE/4.6/kdebase/workspace/; revision=1217284

commit aee1a5ab9f7800ff22a7be6f447fc9bf8f801d8b
Author: Script Kiddy <scripty@kde.org>
Date:   Tue Jan 25 13:53:56 2011 +0000

    SVN_SILENT made messages (.desktop file)
    
    svn path=/branches/KDE/4.6/kdebase/workspace/; revision=1217036

commit 2156981ce5697af3619b4c2942f0110e050d7bfc
Author: Dario Freddi <drf@kde.org>
Date:   Mon Jan 24 10:19:16 2011 +0000

    CCMAIL: Dirk Mueller <mueller@kde.org>
    CCMAIL: release-team@kde.org
    
    Backporting r1216706
    
    This commit fixes a critical bug in the migrator, hence it should be released with 4.6.0
    
    svn path=/branches/KDE/4.6/kdebase/workspace/; revision=1216709

commit c687d20f0c1ee9b51791c48ac8832c50afdc5fe7
Author: Dario Freddi <drf@kde.org>
Date:   Mon Jan 24 10:17:16 2011 +0000

    CCMAIL: Dirk Mueller <mueller@kde.org>
    CCMAIL: release-team@kde.org
    
    Backporting r1216705
    
    This commit is critical and needs to be released in 4.6.0; otherwise it needs to be reverted. Please add it to the tag or this issue might cause troubles in the future.
    
    svn path=/branches/KDE/4.6/kdebase/workspace/; revision=1216708

commit 4003e16ce2b69f235a5063bc6e26d7a970a68966
Author: Marco Martin <notmart@gmail.com>
Date:   Sat Jan 22 20:33:43 2011 +0000

    backport containment creation when is not possible to recycle one
    
    svn path=/branches/KDE/4.6/kdebase/workspace/; revision=1216357

commit 25b922fcce15f477034917962c1de87e2c047846
Author: Albert Astals Cid <tsdgeos@terra.es>
Date:   Sat Jan 22 19:40:46 2011 +0000

    fix the build
    
    svn path=/branches/KDE/4.6/kdebase/workspace/; revision=1216343

commit 2f1c94853a639b36509ea041eee6ff0dc54edf94
Author: Andriy Rysin <arysin@gmail.com>
Date:   Sat Jan 22 19:26:23 2011 +0000

    Fix X11 defines
    BUG: 263199
    FIXED-IN: 4.6
    
    svn path=/branches/KDE/4.6/kdebase/workspace/; revision=1216341

commit 607831437d9dba99aa1a54065586257d7f00c340
Author: Andriy Rysin <arysin@gmail.com>
Date:   Sat Jan 22 19:16:33 2011 +0000

    Restore old DBus API: org.kde.kxkb back as an alternative and print deprecation warning if used
    BUG: 263006
    
    svn path=/branches/KDE/4.6/kdebase/workspace/; revision=1216339

commit 480238f2ac25eab2ac374cde8528876bd861e829
Author: Script Kiddy <scripty@kde.org>
Date:   Sat Jan 22 12:14:26 2011 +0000

    SVN_SILENT made messages (.desktop file)
    
    svn path=/branches/KDE/4.6/kdebase/workspace/; revision=1216281
    
    The following changes were in SVN, but were removed from git:
    
    M	wallpapers/Media_Life/metadata.desktop

commit a6ca09e69da33942e59e8c8b0ed49f2e8f2db0ca
Author: Aaron J. Seigo <aseigo@kde.org>
Date:   Fri Jan 21 15:44:47 2011 +0000

    always reset the cursor, and do so before the drag
    BUG:263824
    
    svn path=/branches/KDE/4.6/kdebase/workspace/; revision=1216150

commit 91f7196ca83d3bb4ef014c531766c290cab9d587
Author: Script Kiddy <scripty@kde.org>
Date:   Fri Jan 21 12:02:04 2011 +0000

    SVN_SILENT made messages (.desktop file)
    
    svn path=/branches/KDE/4.6/kdebase/workspace/; revision=1216106

commit f94ddffc8d4bf45ccff9bf2afd2cb0a7b4fcff30
Author: Jonathan Riddell <jr@jriddell.org>
Date:   Fri Jan 21 00:01:03 2011 +0000

    Switch to 4.6 theme horos
    
    svn path=/branches/KDE/4.6/kdebase/workspace/; revision=1216050
