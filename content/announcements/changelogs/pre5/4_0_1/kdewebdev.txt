2008-01-05 01:42 +0000 [r757474]  mueller

	* branches/KDE/4.0/kdewebdev/doc/CMakeLists.txt: remove
	  non-released docs

2008-01-29 16:27 +0000 [r768248]  mojo

	* branches/KDE/4.0/kdewebdev/klinkstatus/README,
	  branches/KDE/4.0/kdewebdev/klinkstatus/src/engine/searchmanager.cpp,
	  branches/KDE/4.0/kdewebdev/klinkstatus/src/engine/linkchecker.cpp,
	  branches/KDE/4.0/kdewebdev/COPYING.LIB,
	  branches/KDE/4.0/kdewebdev/COPYING-DOCS (removed),
	  branches/KDE/4.0/kdewebdev/klinkstatus/src/ui/sessionwidgetbase.ui,
	  branches/KDE/4.0/kdewebdev/klinkstatus/TODO,
	  branches/KDE/4.0/kdewebdev/klinkstatus/INSTALL,
	  branches/KDE/4.0/kdewebdev/klinkstatus/ChangeLog: Merge with
	  trunk until revision 768226

