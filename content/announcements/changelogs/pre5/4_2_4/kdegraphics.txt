------------------------------------------------------------------------
r961995 | pino | 2009-05-01 11:47:59 +0000 (Fri, 01 May 2009) | 4 lines

backport:
export the actual page numbers, instead of the page count
CCBUG: 191223

------------------------------------------------------------------------
r963720 | gateau | 2009-05-05 10:47:08 +0000 (Tue, 05 May 2009) | 1 line

Bumped version number.
------------------------------------------------------------------------
r963957 | sars | 2009-05-05 18:03:37 +0000 (Tue, 05 May 2009) | 6 lines

Add sane_close() to the destructor.

This is done to allow backends to cleanup (like shutting off lamps)

CCBUG: 190946

------------------------------------------------------------------------
r964592 | pino | 2009-05-06 23:02:29 +0000 (Wed, 06 May 2009) | 5 lines

backport:
do not hardcode the maximum number of digits of the pages line edit to 4
actually, do not hardcode it at all, as the validator will do the job just fine
CCBUG: 191859

------------------------------------------------------------------------
r965803 | sars | 2009-05-09 20:14:22 +0000 (Sat, 09 May 2009) | 1 line

disable the usage of unreleased sane API (release 1.0.20 broke the new API test)
------------------------------------------------------------------------
r966490 | scripty | 2009-05-11 09:06:47 +0000 (Mon, 11 May 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r967307 | scripty | 2009-05-13 08:04:58 +0000 (Wed, 13 May 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r967639 | darioandres | 2009-05-13 16:43:54 +0000 (Wed, 13 May 2009) | 13 lines

Backport to 4.2branch of:
SVN commit 967638 by darioandres:

When updating the gamma values, first search for the "xorg.conf" file
which is the default config file in this moment. Then, fallback to
"XF86Config"
This solves an issue on some distributions as OpenSUSE which creates both
xorg.conf and XF86Config. And the gamma values were only applied to XF86Config
which is not used.

CCBUG: 192484


------------------------------------------------------------------------
r969180 | darioandres | 2009-05-17 15:37:59 +0000 (Sun, 17 May 2009) | 13 lines

Backport to 4.2branch:

SVN commit 969179 by darioandres:

Don't force the inline preview in the Save filedialog.
It doesn't allow the filedialog settings to be saved properly 
and all the user choices are discarded.
(this should be analyzed properly on KDirOperator)

Anyways it is better to let the user choose the behavior they want.

CCBUG: 179317

------------------------------------------------------------------------
r972060 | scripty | 2009-05-24 07:42:39 +0000 (Sun, 24 May 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r972331 | pino | 2009-05-24 16:10:50 +0000 (Sun, 24 May 2009) | 3 lines

backport: use the proper "page-zoom" icon for the zoom tool and the zoom selector
CCBUG: 193770

------------------------------------------------------------------------
r972754 | pino | 2009-05-25 15:33:58 +0000 (Mon, 25 May 2009) | 4 lines

backport: remember to always intialize your vars; m_init, in this case
will be in kde 4.2.4
CCBUG: 192293

------------------------------------------------------------------------
r973213 | pino | 2009-05-26 15:00:29 +0000 (Tue, 26 May 2009) | 2 lines

SVN_SILENT backport stdout spam reduction

------------------------------------------------------------------------
r973214 | pino | 2009-05-26 15:02:17 +0000 (Tue, 26 May 2009) | 3 lines

backport: read the font-style property of styles
CCBUG: 193916

------------------------------------------------------------------------
r973216 | pino | 2009-05-26 15:04:56 +0000 (Tue, 26 May 2009) | 5 lines

backport: properly calculate the page size, converting from points to pixels using the current screen dpi
CCBUG: 171463
CCBUG: 193916
CCBUG: 193542

------------------------------------------------------------------------
r973220 | pino | 2009-05-26 15:11:56 +0000 (Tue, 26 May 2009) | 2 lines

backport: do not leak the dvi renderer when the loading fails

------------------------------------------------------------------------
r973221 | pino | 2009-05-26 15:13:43 +0000 (Tue, 26 May 2009) | 2 lines

SVN_SILENT reduce debug output

------------------------------------------------------------------------
r973223 | pino | 2009-05-26 15:17:25 +0000 (Tue, 26 May 2009) | 2 lines

backport: implement <strikethrough> in <p> tags

------------------------------------------------------------------------
r973252 | pino | 2009-05-26 17:24:29 +0000 (Tue, 26 May 2009) | 2 lines

backport: 0 is a valid value for font style, so use -1 as "not exlicitly set"

------------------------------------------------------------------------
r973394 | pino | 2009-05-27 01:09:50 +0000 (Wed, 27 May 2009) | 4 lines

backport dvi document validity check
please test once kde 4.2.4 is out
CCBUG: 189623

------------------------------------------------------------------------
r973395 | pino | 2009-05-27 01:11:36 +0000 (Wed, 27 May 2009) | 2 lines

bump versions for okular 0.8.4

------------------------------------------------------------------------
r973396 | pino | 2009-05-27 01:12:11 +0000 (Wed, 27 May 2009) | 2 lines

bump okular version to 0.8.4

------------------------------------------------------------------------
