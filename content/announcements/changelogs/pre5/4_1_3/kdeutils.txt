2008-09-26 21:32 +0000 [r865169]  bettio <bettio@localhost>:

	* branches/KDE/4.1/kdeutils/kdf/kdfwidget.cpp: backported capacity
	  bar fix.

2008-09-26 21:42 +0000 [r865170]  bettio <bettio@localhost>:

	* branches/KDE/4.1/kdeutils/kdf/disks.cpp,
	  branches/KDE/4.1/kdeutils/kdf/kdfwidget.cpp: backported device
	  icons fix.

2008-09-27 15:56 +0000 [r865442]  lueck <lueck@localhost>:

	* branches/KDE/4.1/kdeutils/doc/kcontrol/blockdevices/index.docbook,
	  branches/KDE/4.1/kdeutils/doc/okteta/basics.docbook:
	  documentation backport for 4.1.3 from trunk

2008-10-05 05:51 +0000 [r867982]  scripty <scripty@localhost>:

	* branches/KDE/4.1/kdeutils/kgpg/encryptfolder.desktop: SVN_SILENT
	  made messages (.desktop file)

2008-10-14 14:48 +0000 [r871326]  kossebau <kossebau@localhost>:

	* branches/KDE/4.1/kdeutils/okteta/program/oktetakakao/controllers/view/poddecoder/kprimitivetypesview.cpp:
	  fixed: calculation of the width of the value fields was too
	  primitive QLineEdit should get a function "QSize sizeFor( const
	  QString & ) const" or similar. And having to set
	  QStyleOptionFrameV2 manually oneself sucks a little, too. First
	  time in years I am unsatisfied with Qt, oh dear. I still guess I
	  overlooked something ;) CCBUG: 172324

2008-10-14 21:27 +0000 [r871421]  dakon <dakon@localhost>:

	* branches/KDE/4.1/kdeutils/kgpg/kgpginterface.cpp: warning--
	  Backport of r856521 by mlaurent

2008-10-15 07:07 +0000 [r871560]  scripty <scripty@localhost>:

	* branches/KDE/4.1/kdeutils/ktimer/ktimer.desktop: SVN_SILENT made
	  messages (.desktop file)

2008-10-15 21:58 +0000 [r871887]  dakon <dakon@localhost>:

	* branches/KDE/4.1/kdeutils/kgpg/core/emailvalidator.cpp: Fix
	  regular expression for domain names BUG:172850

2008-10-16 05:43 +0000 [r871927]  dakon <dakon@localhost>:

	* branches/KDE/4.1/kdeutils/kgpg/core/emailvalidator.cpp: for top
	  level domains to have at least two characters

2008-10-20 17:51 +0000 [r874061]  dakon <dakon@localhost>:

	* trunk/KDE/kdeutils/kgpg/keysmanager.cpp,
	  branches/KDE/4.1/kdeutils/kgpg/keysmanager.cpp: save changes
	  after editing a key group

2008-10-20 21:25 +0000 [r874169-874165]  sengels <sengels@localhost>:

	* branches/KDE/4.1/kdeutils/ark/plugins/libzipplugin/CMakeLists.txt,
	  branches/KDE/4.1/kdeutils/ark/plugins/libzipplugin/zipplugin.cpp:
	  fix windows building

	* branches/KDE/4.1/kdeutils/ark/plugins/libarchive/libarchivehandler.cpp:
	  fix msvc issue with WChars

	* branches/KDE/4.1/kdeutils/cmake/modules/FindLibArchive.cmake,
	  branches/KDE/4.1/kdeutils/cmake/modules/FindLibZip.cmake: find
	  libzip and libarchive also on msvc

	* branches/KDE/4.1/kdeutils/ark/plugins/libzipplugin/zipplugin.cpp:
	  SVN_SILENT: remove debug code

2008-10-22 22:56 +0000 [r874984]  dakon <dakon@localhost>:

	* branches/KDE/4.1/kdeutils/kgpg/keylistproxymodel.cpp: Fix sorting
	  by trust showing the secret pairs at the wrong end of the list
	  backport of r874983 CCBUG:173328

2008-10-27 08:57 +0000 [r876356]  dakon <dakon@localhost>:

	* trunk/KDE/kdeutils/kgpg/keysmanager.cpp,
	  branches/KDE/4.1/kdeutils/kgpg/keysmanager.cpp: fix crash when
	  trying to show key properties of a group member

2008-10-28 09:32 +0000 [r876855]  mleupold <mleupold@localhost>:

	* branches/KDE/4.1/kdeutils/kwallet/main.cpp: Backport r872485:
	  Proper mime-type.

