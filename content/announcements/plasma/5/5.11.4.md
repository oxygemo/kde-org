---
aliases:
- ../../plasma-5.11.4
changelog: 5.11.3-5.11.4
date: 2017-11-28
layout: plasma
youtube: nMFDrBIA0PM
figure:
  src: /announcements/plasma/5/5.11.0/plasma-5.11.png
  class: text-center mt-4
asBugfix: true
---

- KSysGuard: Use OCS to retrieve Tabs from the KDE store. <a href="https://commits.kde.org/ksysguard/fdead6c30886e17ea0e590df2e7ddb79652fb328">Commit.</a> Fixes bug <a href="https://bugs.kde.org/338669">#338669</a>. Phabricator Code review <a href="https://phabricator.kde.org/D8734">D8734</a>
- Be more explicit about Qt5::Widgets dependencies. <a href="https://commits.kde.org/plasma-desktop/1436ce4c23f8e0bc2e4a1efca73a9a436c34331a">Commit.</a>
- Discover: Don't crash if we get into a weird state. <a href="https://commits.kde.org/discover/63fbc9bf5ef1cbf7fa4743770f032fbe342aa53f">Commit.</a> Fixes bug <a href="https://bugs.kde.org/385637">#385637</a>
