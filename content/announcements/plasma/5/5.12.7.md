---
aliases:
- ../../plasma-5.12.7
changelog: 5.12.6-5.12.7
date: 2018-09-25
layout: plasma
youtube: xha6DJ_v1E4
figure:
  src: /announcements/plasma/5/5.12.0/plasma-5.12.png
  class: text-center mt-4
asBugfix: true
---

- Prevent paste in screen locker. <a href="https://commits.kde.org/kscreenlocker/1638db3fefcae76f27f889b3709521b608aa67ad">Commit.</a> Fixes bug <a href="https://bugs.kde.org/388049">#388049</a>. Phabricator Code review <a href="https://phabricator.kde.org/D14924">D14924</a>
- Improve arrow key navigation of Kicker search results. <a href="https://commits.kde.org/plasma-desktop/1692ae244bc5229df78df2d5ba2e76418362cb50">Commit.</a> Fixes bug <a href="https://bugs.kde.org/397779">#397779</a>. Phabricator Code review <a href="https://phabricator.kde.org/D15286">D15286</a>
- Fix QFileDialog not remembering the last visited directory. <a href="https://commits.kde.org/plasma-integration/4848bec177b2527662098f97aa745bb839bc15e3">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D14437">D14437</a>
