---
aliases:
- ../../plasma-5.8.8
changelog: 5.8.7-5.8.8
date: 2017-10-25
layout: plasma
youtube: LgH1Clgr-uE
figure:
  src: /announcements/plasma/5/5.8.0/plasma-5.8.png
  class: text-center mt-4
asBugfix: true
---

- Fix crash when using Discover during updates. <a href="https://commits.kde.org/discover/aa12b3c822354e0f526dccff7f1c52c26fb35073">Commit.</a> Fixes bug <a href="https://bugs.kde.org/370906">#370906</a>
- Try to read CPU clock from cpufreq/scaling_cur_freq instead of /proc/cpuinfo. <a href="https://commits.kde.org/ksysguard/cbaaf5f4ff54e20cb8ec782737e04d540085e6af">Commit.</a> Fixes bug <a href="https://bugs.kde.org/382561">#382561</a>. Phabricator Code review <a href="https://phabricator.kde.org/D8153">D8153</a>
- DrKonqi crash handler: make attaching backtraces to existing bug reports work. <a href="https://commits.kde.org/plasma-workspace/774a23fc0a7c112f86193ee6e07947fee6282ef4">Commit.</a>
- Look and Feel/Notifications. Fix crash when removing panel with system tray in it. <a href="https://commits.kde.org/plasma-workspace/8a05294e5b3ef1df86f099edde837b8c8d28ccaf">Commit.</a> Fixes bug <a href="https://bugs.kde.org/378508">#378508</a>. Phabricator Code review <a href="https://phabricator.kde.org/D6653">D6653</a>
