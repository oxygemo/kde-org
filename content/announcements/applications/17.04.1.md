---
aliases:
- ../announce-applications-17.04.1
changelog: true
date: 2017-05-11
description: KDE Ships KDE Applications 17.04.1
layout: application
title: KDE Ships KDE Applications 17.04.1
version: 17.04.1
---

{{% i18n_var "May 11, 2017. Today KDE released the first stability update for <a href='%[1]s'>KDE Applications 17.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone." "../17.04.0" %}}

More than 20 recorded bugfixes include improvements to kdepim, dolphin, gwenview, kate, kdenlive, among others.

{{% i18n_var "This release also includes Long Term Support version of KDE Development Platform %[1]s." "4.14.32" %}}