---
aliases:
- ../announce-applications-16.08.2
changelog: true
date: 2016-10-13
description: KDE Ships KDE Applications 16.08.2
layout: application
title: KDE Ships KDE Applications 16.08.2
version: 16.08.2
---

{{% i18n_var "October 13, 2016. Today KDE released the second stability update for <a href='%[1]s'>KDE Applications 16.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone." "../16.08.0" %}}

More than 30 recorded bugfixes include improvements to kdepim, ark, dolphin, kgpg, kolourpaint, okular, among others.

{{% i18n_var "This release also includes Long Term Support version of KDE Development Platform %[1]s." "4.14.25" %}}