<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top"><th align="left">Location</th><th align="left">Size</th><th align="left">MD5&nbsp;Sum</th></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.93/src/kdeaccessibility-3.93.0.tar.bz2">kdeaccessibility-3.93.0</a></td><td align="right">7.4MB</td><td><tt>1ecdcb69faec6d4b17b2a957a0c80770</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.93/src/kdeaddons-3.93.0.tar.bz2">kdeaddons-3.93.0</a></td><td align="right">764KB</td><td><tt>552e2d51c2b186459922abbff8d7fe37</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.93/src/kdeadmin-3.93.0.tar.bz2">kdeadmin-3.93.0</a></td><td align="right">1.4MB</td><td><tt>d0a07baac73e818160836b482a1b39aa</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.93/src/kdeartwork-3.93.0.tar.bz2">kdeartwork-3.93.0</a></td><td align="right">44MB</td><td><tt>0a001c9bd0cf8970323a5e9232ea0ae4</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.93/src/kdebase-3.93.0.tar.bz2">kdebase-3.93.0</a></td><td align="right">60MB</td><td><tt>869c6ff7d9b9c359b31753306ba4c57c</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.93/src/kdebase-workspace-3.93.0.tar.bz2">kdebase-workspace-3.93.0</a></td><td align="right">8.9MB</td><td><tt>d9261bc596e01b15a2dbdf7ab2575c04</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.93/src/kdebindings-3.93.0.tar.bz2">kdebindings-3.93.0</a></td><td align="right">2.1MB</td><td><tt>d88a2a85c1dc12e8953107607ed82d48</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.93/src/kdeedu-3.93.0.tar.bz2">kdeedu-3.93.0</a></td><td align="right">37MB</td><td><tt>f468d0a4ced0b59f571af88bf78297bf</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.93/src/kdegames-3.93.0.tar.bz2">kdegames-3.93.0</a></td><td align="right">24MB</td><td><tt>5362ed6baa4b3a6e12c2af91916c543f</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.93/src/kdegraphics-3.93.0.tar.bz2">kdegraphics-3.93.0</a></td><td align="right">2.4MB</td><td><tt>2e574f3d60664f5f9fe26302be47f9e6</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.93/src/kdelibs-3.93.0.tar.bz2">kdelibs-3.93.0</a></td><td align="right">9.0MB</td><td><tt>dac92cdd69bd08d5877e288e7371499d</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.93/src/kdemultimedia-3.93.0.tar.bz2">kdemultimedia-3.93.0</a></td><td align="right">2.8MB</td><td><tt>4b9634da956a8f86be4a9e40c31f16d8</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.93/src/kdenetwork-3.93.0.tar.bz2">kdenetwork-3.93.0</a></td><td align="right">2.0MB</td><td><tt>c1203e0903b0856cdf2f0377943a1ab9</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.93/src/kdepim-3.93.0.tar.bz2">kdepim-3.93.0</a></td><td align="right">13MB</td><td><tt>9275f82cd2666b7f891d0d35c22f9804</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.93/src/kdepimlibs-3.93.0.tar.bz2">kdepimlibs-3.93.0</a></td><td align="right">1.6MB</td><td><tt>488e70022a2f0a3922621de2af9b7ddd</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.93/src/kdesdk-3.93.0.tar.bz2">kdesdk-3.93.0</a></td><td align="right">5.0MB</td><td><tt>4197f4949b773dd9cadd73459b2accb8</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.93/src/kdetoys-3.93.0.tar.bz2">kdetoys-3.93.0</a></td><td align="right">2.2MB</td><td><tt>3d1ca69b93e64c9daa5150f3a27a2383</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.93/src/kdeutils-3.93.0.tar.bz2">kdeutils-3.93.0</a></td><td align="right">2.2MB</td><td><tt>c5ed7d5014e6da4bec0ae846db14f596</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/koffice-1.9.93/src/koffice-1.9.93.tar.bz2">koffice-1.9.93</a></td><td align="right">59MB</td><td><tt>9bca1899481aa1fdf38493c6de1ba1c9</tt></td></tr>
</table>
