<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top"><th align="left">Location</th><th align="left">Size</th><th align="left">MD5&nbsp;Sum</th></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.80.1/src/kdelibs-3.80.1.tar.bz2">kdelibs-3.80.1</a></td><td align="right">13MB</td><td><tt>ac1e42d0aad01bc8f8d6af39c3070a8a</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.80.1/src/kdepimlibs-3.80.1.tar.bz2">kdepimlibs-3.80.1</a></td><td align="right">1.1MB</td><td><tt>6b5114f7023de02523f429ca11e32dcf</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/3.80.1/src/kdebase-3.80.1.tar.bz2">kdebase-3.80.1</a></td><td align="right">18MB</td><td><tt>b5c9d67ec74c5a2b2c12ebf7862d57ab</tt></td></tr>
</table>
