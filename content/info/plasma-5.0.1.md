---
version: "5.0.1"
title: "Plasma 5.0.1 Source Information Page"
errata:
    link: https://community.kde.org/Plasma/5.0_Errata
    name: 5.0 Errata
announcement: /announcements/plasma5.0.1
bugfix: true
type: info/plasma5
---


This is a bugfix release of Plasma, featuring Plasma Desktop and
other essential software for your desktop computer.  Details in the <a
href="/announcements/plasma-5.0.1">Plasma 5.0.1 announcement</a>.

For new features read the initial release <a
href="/announcements/plasma5.0/">Plasma 5.0 announcement</a>.
