KDE Project Security Advisory
=============================

Title:          kio_fish stores the typed password in KWallet even if the user doesn't check the "Remember" box.
Risk Rating:    Low
CVE:            CVE-2020-12755
Versions:       kio-extras - all versions until 20.04.0
Date:           10th May 2020


Overview
========
fishProtocol::establishConnection in fish/fish.cpp in KDE kio-extras through 20.04.0
makes a cacheAuthentication call even if the user had not set the keepPassword option.
This may lead to unintended KWallet storage of the password.

This is considered a security issue by users who do not trust KWallet (e.g. because
passwords can be read in KWalletManager, given physical access).

Solution
========
- Update to kio-extras >= 20.04.1
- or apply the following patch:
https://commits.kde.org/kio-extras/d813cef3cecdec9af1532a40d677a203ff979145


Workarounds
===========
* Using SSH keys rather than passwords.
* Using sftp:// instead of fish://
* Deleting the password from KWallet using kwalletmanager.


Credits
=======
Thanks to Johannes Hofmann for reporting the issue, to Albert Astals Cid for
the initial reproduction and investigation, and to David Faure for the fix.

