-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1


KDE Security Advisory: Konqueror Cross Site Scripting Vulnerability 
Original Release Date: 2002-09-08
URL: http://www.kde.org/info/security/advisory-20020908-2.txt

0. References
        http://online.securityfocus.com/archive/1/290710/2002-09-03/2002-09-09/0

1. Systems affected:

        KDE 2.2.2
        KDE 3.0 - 3.0.3 

2. Overview:
            
        Konqueror's cross Site scripting protection fails to initialize the 
        domains on sub-(i)frames correctly. As a result, Javascript can 
        access any foreign subframe which is defined in the HTML source. 

3. Impact:
        
        Users of Konqueror and other KDE software that uses the KHTML 
        rendering engine may fall victim of a cookie stealing and 
        other cross site scripting attacks. 
   
4. Solution:
        
        Apply the appended patch to kdelibs, update to the kdelibs-3.0.3a or, 
        as a workaround, disable Javascript or cookies.     

        kdelibs-3.0.3a can be downloaded from 
        http://download.kde.org/stable/3.0.3 :

        02627f595af113f7d544561a7ff6ec85  kdelibs-3.0.3a.tar.bz2
       

5. Patch:

        A patch for KDE 3.0.3 is available from
        
        ftp://ftp.kde.org/pub/kde/security_patches :
  
        523b2fb677310792cbb04861f358d08d  post-3.0.3-kdelibs-khtml.diff

        A patch for KDE 2.2.2 is available from
   
        ftp://ftp.kde.org/pub/kde/security_patches : 
 
        b0b23c3caa062c60375a1160418a2810  post-2.2.2-kdelibs-khtml.diff


-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.0.7 (GNU/Linux)

iD8DBQE9fntPvsXr+iuy1UoRAiDrAKCIgT/f7UvBqXdgPVkGeFvNktSagQCgkUMw
lxtwL9WYkKyR7TcrK7yY36M=
=yQpt
-----END PGP SIGNATURE-----
